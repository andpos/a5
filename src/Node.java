
import java.util.*;

public class Node {

   private String name;
   private Node firstChild;
   private Node nextSibling;

   Node(String name, Node firstChild, Node nextSibling) {
      this.name = name;
      this.firstChild = firstChild;
      this.nextSibling = nextSibling;
   }

   private static Node parseNodeFromTokenizer(StringTokenizer st) {
      String name = null;
      Node firstChild = null;
      Node nextSibling = null;

      loop:
      while (st.hasMoreTokens()) {
         String token = st.nextToken();
         switch (token.charAt(0)) {
            case '(':
               firstChild = parseNodeFromTokenizer(st);
               break;
            case ')':
               break loop;
            case ',':
               nextSibling = parseNodeFromTokenizer(st);
               break loop;
            case ' ':
               break; // ignore spaces
            default:
               if (name != null) {
                  throw new RuntimeException(String.format("Unable to assign name \"%s\" to a node, because it already has name \"%s\"", token, name));
               }
               name = token;
               break;
         }
      }

      if (name == null) {
         throw new RuntimeException("Missing node name");
      }
      return new Node(name, firstChild, nextSibling);
   }
   
   public static Node parsePostfix(String s) {
      if (s == null) {
         throw new IllegalArgumentException("Tree expression is null");
      }

      StringTokenizer st = new StringTokenizer(s.replaceAll("\\s", " "), "(, )", true);
      if (!st.hasMoreTokens()) {
         throw new IllegalArgumentException("Tree expression is empty or contains only whitespaces");
      }

      Node rootNode;
      try {
         rootNode = parseNodeFromTokenizer(st);
      } catch (Exception e) {
         throw new RuntimeException(String.format("Parsing of tree expression \"%s\" failed with error: %s", s, e.getMessage()));
      }

      if (rootNode.nextSibling != null ) {
         throw new RuntimeException(String.format("The root node of tree expression must not have any siblings (expression: \"%s\")", s));
      }

      return rootNode;
   }

   public String leftParentheticRepresentation() {
      StringBuilder sb = new StringBuilder();

      Node currentSibling = this;
      do {
         sb.append(currentSibling.name);
         if (currentSibling.firstChild != null) {
            sb.append('(');
            sb.append(currentSibling.firstChild.leftParentheticRepresentation());
            sb.append(')');
         }
         sb.append(',');
         currentSibling = currentSibling.nextSibling;
      } while (currentSibling != null);

      sb.setLength(sb.length() - 1); // remove trailing comma
      return sb.toString();
   }

   public static void main (String[] param) {
      String s = "(B1,C)A";
      Node t = Node.parsePostfix (s);
      String v = t.leftParentheticRepresentation();
      System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)

      s = "A(B1,C)";
      t = Node.parsePostfix (s);
      v = t.leftParentheticRepresentation();
      System.out.println (s + " ==> " + v); // A(B1,C) ==> A(B1,C)
   }
}
